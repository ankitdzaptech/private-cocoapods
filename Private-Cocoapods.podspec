Pod::Spec.new do |s|
  s.name     = 'Private-Cocoapods'
  s.version  = '1.0.0'
  s.license  = 'ABC'
  s.summary  = 'A delightful iOS framework.'
  s.homepage = 'http://goldenflag.com/'
  s.authors  = { 'Chinab' => 'chinab@zaptechsolutions.com', 'Ankit' => 'ankitd@zaptechsolutions.com' }
  s.source   = { :git => 'https://bitbucket.org/ankitdzaptech/private-cocoapods.git', :tag => '1.0.0' }
  s.source_files = 'Private-Cocoapods'
  s.requires_arc = true

  s.ios.deployment_target = '7.0'

end