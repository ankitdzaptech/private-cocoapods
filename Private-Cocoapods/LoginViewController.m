//
//  LoginViewController.m
//  GFCommon
//
//  Created by Chinab on 5/9/14.
//  Copyright (c) 2014 Golden Flag America, LLC. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@property BOOL isLoginViewUP;
@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


# pragma mark - UITextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.isLoginViewUP == NO) {
        self.isLoginViewUP = YES;
        const int movementDistance = 105;
        const float movementDuration = 0.3f;
        int movement = (self.isLoginViewUP ? -movementDistance : movementDistance);
        [UIView beginAnimations: @"animation" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.loginView.frame = CGRectOffset(self.loginView.frame, 0, movement);
        [UIView commitAnimations];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (self.isLoginViewUP == YES) {
        self.isLoginViewUP = NO;
        const int movementDistance = 105;
        const float movementDuration = 0.3f;
        int movement = (self.isLoginViewUP ? -movementDistance : movementDistance);
        [UIView beginAnimations: @"animation" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.loginView.frame = CGRectOffset(self.loginView.frame, 0, movement);
        [UIView commitAnimations];
    }
    return YES;
}

- (IBAction)loginButtonTapped:(id)sender {
    NSLog(@"Login button tapped");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registerButtonTapped:(id)sender {
    NSLog(@"Register button tapped");
}

# pragma mark - Orientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
        if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            self.backgroundImage.image = [UIImage imageNamed:@"LoginBg_Portrait_iPad.png"];
        } else if(toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
            self.backgroundImage.image = [UIImage imageNamed:@"LoginBg_Landscape_iPad.png"];
        }
    }
}

@end
