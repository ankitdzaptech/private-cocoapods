//
//  AppDelegate.h
//  GFCommonDemo
//
//  Created by MAC on 5/13/14.
//  Copyright (c) 2014 Golden Flag America, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
